import { createStore } from "vuex";
import axios from 'axios'
import createPersistedState from 'vuex-persistedstate'
export default createStore({
    plugins: [createPersistedState()],
    state: {
        tokens: {
            access_token: null,
            expires_in: null,
            refresh_token: null,
            token_type: null,
        },
        currentUser: {
            user_id: null,
            name: null,
            email: null,
            company_id: null,
            branch_id: null,
            sec_user_id: null
        }
    },
    mutations: {
        updateTokens(state, tokens) {
            state.tokens = tokens
        },
        updateCurrentUser(state, currentUser) {
            state.currentUser = currentUser
        },
    },
    actions: {
        login(context, user) {
            return new Promise((resolve, reject) => {
                let data = {
                    username: user.email,
                    password: user.password,
                };
                axios.post('/api/authenticate', data)
                    .then(response => {
                        let responseData = response.data;
                        let now = Date.now();

                        responseData.expires_in = responseData.expires_in + now;

                        context.commit('updateTokens', responseData);

                        axios.defaults.headers.common['Authorization'] = `Bearer ${responseData.access_token}`;

                        resolve(response)
                    })
                    .catch(response => {
                        reject(response)
                    })
            })
        },
        logout(context) {
            axios.get('/api/logout')
                .then(response => {
                    if (response.data.statusCode === 200) {
                        axios.defaults.headers.common['Authorization'] = null;
                        context.commit('updateTokens', response.data);
                        window.location.href = '/';
                    }
                });
        },
        getCurrentUser(context) {
            return new Promise((resolve, reject) => {
                axios.get('/api/getCurrentUser')
                    .then(response => {
                        let responseData = response.data.data;
                        context.commit('updateCurrentUser', responseData);
                        resolve(response);
                    })
                    .catch(response => {
                        reject(response);
                    })
            })
        },
    },
    modules: {},
});