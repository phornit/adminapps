import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from 'axios';

//import bootstrap css & js
import 'bootstrap/dist/css/bootstrap.css'
import 'admin-lte/dist/css/adminlte.css';
import 'admin-lte/dist/css/adminlte.min.css';

axios.defaults.baseURL = 'http://www.mdo-cambodia.org';

axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common['accept'] = 'application/json';
axios.defaults.headers.common['X-API-KEY'] = '1ccbc4c913bc4ce785a0a2de444aa0d6';

let access_token = store.getters.getAccessToken;

if (access_token)
    axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}`;

createApp(App).use(store).use(router).mount("#app");